package nl.bioinf;

import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Arrays;

public class WekaRunner {
    public static void main(final String[] args) {
        try {
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(args);
            if (op.helpRequested()) {
                op.printHelp();
                return;
            }
            if (op.getFileName() == null) {
                op.printNoFile();
                return;
            }

            WekaRunner runner = new WekaRunner();
            runner.start(op.getFileName());

        } catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed. Reason: " + ex.getMessage());
            ApacheCliOptionsProvider op = new ApacheCliOptionsProvider(new String[]{});
            op.printHelp();
        }
    }

    private void start(final String unknownFile) {
        try {
            J48 fromFile = loadClassifier();
            Instances unknownInstances;
            if (unknownFile.endsWith(".arff")) {
                unknownInstances = loadArff(unknownFile);
            }
            else if (unknownFile.endsWith(".csv")) {
                CSVLoader csvLoader = new CSVLoader();
                csvLoader.setSource(new File(unknownFile));
                csvLoader.setNominalAttributes("first-last");
                unknownInstances = csvLoader.getDataSet();
                if (unknownInstances.classIndex() == -1)
                    unknownInstances.setClassIndex(unknownInstances.numAttributes() - 1);
            }
            else{
                throw new UnsupportedOperationException("No known file format");
            }
            Instances newData = new Instances(unknownInstances);
//            Instances newData;
            // 1. nominal attribute
            Add filter = new Add();
            filter.setAttributeIndex("last");
            filter.setNominalLabels("TRUE,FALSE");
            filter.setAttributeName("edible");
            filter.setInputFormat(unknownInstances);
            newData = Filter.useFilter(unknownInstances, filter);

            newData.setClassIndex(newData.numAttributes() - 1);
            System.out.println("newData # attributes = " + newData.numAttributes());
            System.out.println("newData class index= " + newData.classIndex());

            System.out.println("unknownInstancesFromCsv = " + newData);

            classifyNewInstance(fromFile, newData);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void classifyNewInstance(J48 tree, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = tree.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }

    private J48 loadClassifier() throws Exception {
        // deserialize model
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File("../../testdata/j48.model");
        return (J48) weka.core.SerializationHelper.read(file.getAbsolutePath());
    }

    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }
}
